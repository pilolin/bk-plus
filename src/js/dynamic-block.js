import { debounce } from './helper';

if (document.querySelector('.dynamic-blocks')) {
    if (window.matchMedia('(min-width: 1201px)').matches) {
        window.addEventListener('load', () => {
            updateDynamicBlocks();
        });
    }

    const debUpdateDynamicBlocks = debounce(updateDynamicBlocks, 300);

    window.addEventListener('resize', () => {
        if (window.matchMedia('(min-width: 1201px)').matches) {
            debUpdateDynamicBlocks();
        }
    });
}

function updateDynamicBlocks() {
    const block1 = document.querySelector('.dynamic-block-1');
    const block2 = document.querySelector('.dynamic-block-2');
    const block3 = document.querySelector('.dynamic-block-3');
    const block4 = document.querySelector('.dynamic-block-4');

    const topBlock1 = 60;
    const leftBlock1 = -50;
    block1.style.top = `${topBlock1}px`;
    block1.style.left = `${leftBlock1}px`;

    const topBlock2 = 0;
    const leftBlock2 = block1.getBoundingClientRect().width - 176 + leftBlock1;
    block2.style.top = `${topBlock2}px`;
    block2.style.left = `${leftBlock2}px`;

    const topBlock3 = block1.getBoundingClientRect().height + 190
    const leftBlock3 = -50;
    block3.style.top = `${topBlock3}px`;
    block3.style.left = `${leftBlock3}px`;

    const topBlock4 = block2.getBoundingClientRect().height - 70 + topBlock2;
    const leftBlock4 = block3.getBoundingClientRect().width - 66 + leftBlock3;
    block4.style.top = `${topBlock4}px`;
    block4.style.left = `${leftBlock4}px`;

    const rectBlock3 = block3.getBoundingClientRect();
    const rectBlock4 = block4.getBoundingClientRect();

    const container = document.querySelector('.dynamic-blocks');
    const sumHeight_1_3 = topBlock3 + rectBlock3.height + 50;
    const sumHeight_2_4 = topBlock4 + rectBlock4.height + 50;

    container.style.height = `${Math.max(sumHeight_1_3, sumHeight_2_4)}px`
}