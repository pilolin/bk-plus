import Inputmask from "inputmask";
import 'mmenu-js/dist/mmenu';
import 'mmenu-js/dist/mmenu.polyfills';
import 'mmenu-js/src/mmenu.scss';
import { Swiper, Navigation, Pagination, Keyboard, Autoplay, EffectFade } from 'swiper';
import 'swiper/swiper.scss';
import 'swiper/components/effect-fade/effect-fade.scss';
import 'swiper/components/navigation/navigation.scss';
import 'swiper/components/pagination/pagination.scss';
import './form-items';
import './smoothScroll';
import MicroModal from 'micromodal';
import './dynamic-block';
import { animate } from "./helper";
import './dropdown';
import './../scss/styles.scss'

Swiper.use([Navigation, Pagination, Keyboard, Autoplay, EffectFade]);

// mask
new Inputmask('+7 (999) 999-99-99', { showMaskOnHover: false }).mask(document.querySelectorAll('input[type="tel"]'));

new Inputmask({
	mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{2,20}.*{2,6}[.*{1,2}]",
	greedy: false,
	showMaskOnHover: false,
	onBeforePaste: function (pastedValue) {
		pastedValue = pastedValue.toLowerCase();
		return pastedValue.replace("mailto:", "");
	},
	definitions: {
		'*': {
			validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
			casing: "lower"
		}
	}
}).mask(document.querySelectorAll('input.email-mask'));

//modal
MicroModal.init({
	openTrigger: 'data-open-modal',
	closeTrigger: 'data-close-modal',
	disableScroll: true,
	awaitCloseAnimation: true,
});

// replace svg icons
window.addEventListener('load', () => {
	[...document.querySelectorAll('svg use')].forEach((element) => {
		const targetSymbol = document.querySelector(element.href.baseVal);
		if (targetSymbol) {
            const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
            const viewBox = targetSymbol.viewBox.baseVal;

            svg.setAttributeNS(null,'viewBox', `${viewBox.x} ${viewBox.y} ${viewBox.width} ${viewBox.height}`);
            svg.setAttributeNS(null,'width', element.parentElement.width.baseVal.value);
            svg.setAttributeNS(null,'height', element.parentElement.height.baseVal.value);
            svg.setAttributeNS(null,'fill', 'none');
            svg.insertAdjacentHTML('afterbegin', targetSymbol.innerHTML);

            element.parentElement.parentNode.replaceChild(svg, element.parentElement);
        }
	});
});

// main slider
window.addEventListener('load', () => {
	const elementNumberCurrentSlide = document.querySelector('.main-slider-nav__number-current-slide')
	const elementTotalSlide = document.querySelector('.main-slider-nav__total-slides')

	new Swiper('.main-slider__container', {
		autoplay: {
			delay: 5000,
		},
		loop: true,
		keyboard: {
			enabled: true,
			onlyInViewport: true,
		},
		effect: 'fade',
		fadeEffect: {
			crossFade: true
		},
		pagination: {
			el: '.swiper-pagination',
			type: 'progressbar',
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		on: {
			init: (swiper) => {
				const quantity = swiper.slides.length / (swiper.params.loop ? 2 : 1);
				elementTotalSlide.innerHTML = quantity < 10 ? `0${quantity}` : quantity;
			},
			slideChange: (swiper) => {
				elementNumberCurrentSlide.innerHTML = (swiper.realIndex + 1) < 10 ? `0${(swiper.realIndex + 1)}` : (swiper.realIndex + 1);
			},
		},
	});

	if (
		window.matchMedia('(max-width: 992px)').matches
		&& document.querySelector('.news-list__slider').classList.contains('.swiper-container-initialized')
	) {
		initNewsSlider();
	}
});

// news
window.addEventListener('resize', function () {
	const newsSlider = document.querySelector('.news-list__slider');

	if (
		window.matchMedia('(max-width: 992px)').matches
		&& newsSlider
		&& !newsSlider.classList.contains('swiper-container-initialized')
	) {
		initNewsSlider();
	}

	if (
		window.matchMedia('(min-width: 993px)').matches
		&& newsSlider
		&& newsSlider.classList.contains('swiper-container-initialized')
	) {
		destroyNewsSlider();
	}
})

// mobile news slider
function initNewsSlider() {
	new Swiper('.news-list__slider', {
		slidesPerView: 1,
		pagination: {
			el: '.swiper-pagination',
			dynamicBullets: true,
		},
		breakpoints: {
			600: {
				slidesPerView: 2
			},
			768: {
				slidesPerView: 3
			},
		}
	});
}

function destroyNewsSlider() {
	document.querySelector('.news-list__slider').swiper.destroy();
}

// init review slider
window.addEventListener('load', () => {
    new Swiper('.review__slider', {
        keyboard: {
            enabled: true,
            onlyInViewport: true,
        },
        effect: 'fade',
        fadeEffect: {
            crossFade: true
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
});

// scroll header
if (document.querySelector('.main-page-body')) {
	const header = document.querySelector('#header')

	window.addEventListener('scroll', () => {
		if (window.pageYOffset > 1) {
			header.classList.add('is-scroll');
		} else {
			header.classList.remove('is-scroll');
		}
	});
}

document.addEventListener('click', (e) => {
	const targetServiceItem = e.target.closest('.service-list-item');

	if (!targetServiceItem) return;

	e.preventDefault();

	const openedServiceItems = document.querySelectorAll('.service-list-item.is-open');
	const targetServiceItemMenu = targetServiceItem.querySelector('.service-list-item__menu');

	if(!targetServiceItem.classList.contains('is-open')) {
		targetServiceItem.classList.add('is-open');
		targetServiceItemMenu.setAttribute('aria-expanded', 'true');
	}

	openedServiceItems.forEach((item) => {
		item.classList.remove('is-open');
		item.querySelector('.service-list-item__menu').setAttribute('aria-expanded', 'false');
	});
});

// animate service menu
document.addEventListener('click', (e) => {
    const targetTitle = e.target.closest('.service-menu-nav-item__header');

    if (!targetTitle) return;

    e.preventDefault();

    const openedItems = document.querySelectorAll('.service-menu-nav-item[aria-expanded="true"]');
    const targetItem = targetTitle.closest('.service-menu-nav-item');

    if(targetItem.getAttribute('aria-expanded') === 'false') {
        const menuList = targetItem.querySelector('.service-menu-nav-item__list');
        const height = menuList.scrollHeight;

        animate({
            duration: 250,
            timing(timeFraction) {
                return timeFraction;
            },
            draw: (progress) => {
                menuList.style.height = `${progress * height}px`;
            },
            onEnd: () => {
                menuList.style.height = 'auto';
                targetItem.setAttribute('aria-expanded', 'true');
            },
        });
    }

    openedItems.forEach((item) => {
        const menuList = item.querySelector('.service-menu-nav-item__list');
        const height = menuList.scrollHeight;

        animate({
            duration: 250,
            timing(timeFraction) {
                return timeFraction;
            },
            draw: (progress) => {
                menuList.style.height = `${(1 - progress) * height}px`;
            },
            onEnd: () => {
                item.setAttribute('aria-expanded', 'false');
            }
        });
    });
});