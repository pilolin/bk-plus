document.addEventListener('DOMContentLoaded', () => {
	var menu = new Mmenu('#mobile-menu', {
		navbar: {
			title: 'Меню'
		},
		extensions: [
			'pagedim-black',
			'border-full',
			'fx-menu-slide',
		]
	});

	var api = menu.API;

	document.querySelector('#mobile-menu-toggle').addEventListener('click', function(e) {
		e.preventDefault();
		api.open();
	});
});