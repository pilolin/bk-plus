// фокус инпута
document.addEventListener('focusin', (e) => {
	if ( !e.target.closest('.form-item__text-field input') ) return;

	e.target.closest('.form-item__text-field').classList.add('focus');
});

// на случай автозаполнения
document.addEventListener('change', (e) => {
	if ( !e.target.closest('.form-item__text-field input') ) return;

	if ( !e.target.value.length ) return;

	e.target.closest('.form-item__text-field').classList.add('focus');
});

// блюр инпута
document.addEventListener('focusout', (e) => {
	if ( !e.target.closest('.form-item__text-field input') ) return;

	const inputElement = e.target.closest('.form-item__text-field input');

	if ( !inputElement.value.length ) {
		inputElement.parentElement.classList.remove('focus');
	}
});

// фокус textarea
document.addEventListener('focusin', (e) => {
	if ( !e.target.closest('.form-item__textarea textarea') ) return;

	e.target.closest('.form-item__textarea').classList.add('focus');
});

// на случай автозаполнения
document.addEventListener('change', (e) => {
	if ( !e.target.closest('.form-item__textarea input') ) return;

	if ( !e.target.value.length ) return;

	e.target.closest('.form-item__textarea').classList.add('focus');
});

// блюр textarea
document.addEventListener('focusout', (e) => {
	if ( !e.target.closest('.form-item__textarea textarea') ) return;

	const textareaElement = e.target.closest('.form-item__textarea textarea');

	if ( !textareaElement.value.length ) {
		textareaElement.parentElement.classList.remove('focus');
	}
});

// анимация лейбла у всех заполненных инпутов
document.addEventListener('DOMContentLoaded', () => {
	const textFieldsOnPage = document.querySelectorAll('.form-item__text-field input');

	for ( const input of [...textFieldsOnPage] ) {
		if ( input.value.length ) {
			input.parentElement.classList.add('focus');
		}
	}
});