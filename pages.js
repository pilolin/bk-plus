const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = [
	new HtmlWebpackPlugin({
		template: './src/index.pug',
		filename: './index.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/services.pug',
		filename: './services.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/service-single.pug',
		filename: './service-single.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/service-single-2.pug',
		filename: './service-single-2.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/contacts.pug',
		filename: './contacts.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/about.pug',
		filename: './about.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/news.pug',
		filename: './news.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/news-single.pug',
		filename: './news-single.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/geo-company.pug',
		filename: './geo-company.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/jobs.pug',
		filename: './jobs.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/command.pug',
		filename: './command.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/modals.pug',
		filename: './modals.html'
	}),
]